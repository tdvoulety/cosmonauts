import { Component, OnInit } from '@angular/core';
import { Cosmonaut } from '../cosmonaut';
import { DataService } from '../data.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {
  cosmonauts: Cosmonaut[];

  constructor(private dataService: DataService) { }

  ngOnInit() {
    this.getCosmonauts();
  }

  getCosmonauts(): void {
    this.dataService.getCosmonauts().subscribe(
      response => this.cosmonauts = response.data
    );
  }
}
