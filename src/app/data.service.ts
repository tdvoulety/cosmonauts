import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';

import { Cosmonaut } from './cosmonaut';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class DataService {

  constructor(private http: HttpClient) { }

  getCosmonauts(): Observable<any> {
    return this.http.get<any>('https://reqres.in/api/users').pipe(
      catchError(this.handleError('getCosmonauts', []))
    );
  }

  addCosmonaut(cosmonaut: Cosmonaut): Observable<any> {
    console.log(cosmonaut)
    return this.http.post<any>('https://reqres.in/api/users', cosmonaut, httpOptions).pipe(
      catchError(this.handleError('addCosmonaut'))
    );
  }

  deleteCosmonaut(id: number) {
    return this.http.delete('https://jsonplaceholder.typicode.com/users' + id, httpOptions);
  }

  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      console.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
}
