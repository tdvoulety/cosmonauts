import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';
import { Cosmonaut } from '../cosmonaut';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss']
})
export class FormComponent implements OnInit {
  public form: Cosmonaut = {
    first_name: '',
    last_name: ''
  };

  constructor(private dataService: DataService) { }

  ngOnInit() {}

  handleSubmit() {
    this.dataService.addCosmonaut(this.form);
  }
}
