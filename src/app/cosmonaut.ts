export class Cosmonaut {
  id?: number;
  first_name: string;
  last_name: string;
}
